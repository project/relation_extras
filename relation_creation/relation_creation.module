<?php

/**
 * Implements hook_menu().
 *
 * Provide a node tab for adding relations.
 *
 * @todo: add tabs for any entity type.
 */
function relation_creation_menu() {
  $items['node/%node/relations'] = array(
    'title' => t('Relations'),
    //'title callback' => 'relation_creation_title_callback',
    //'title arguments' => array(1),
    'page callback' => 'relation_creation_page_callback',
    'page arguments' => array(1),
    'type' => MENU_LOCAL_TASK,
    'access callback' => 'relation_creation_access_callback',
    'access arguments' => array(1, array('create relations')),
  );

  return $items;
}

/**
 * Implements hook_forms().
 *
 * Routes all the various relation type forms to a main form handler.
 */
function relation_creation_forms($form_id, $args) {
  $relation_types = relation_get_types();

  foreach (array_keys($relation_types) as $relation_type) {
    $forms["relation_creation_{$relation_type}_form"] = array(
      'callback' => 'relation_creation_form',
    );
  }

  return $forms;
}

/**
 * Page callback for the relations node tab.
 *
 * Gets an add for for each relation type applicable to the node.
 */
function relation_creation_page_callback($node) {
  $relations = relation_get_available_types('node', $node->type, 'source');

  $page = array();

  foreach ($relations as $relation) {
    $relation_form = drupal_get_form(
      "relation_creation_{$relation->relation_type}_form",
      $relation,
      $node
    );
    $page[$relation->relation_type] = $relation_form;
  }

  return $page;
}

/**
 * Returns a form for adding a relation.
 */
function relation_creation_form($form, &$form_state, $relation) {
  $results = array();
  foreach ($relation->target_bundles as $target) {
    $bundle = explode(":", $target);

    $query = new EntityFieldQuery();
    $query = $query->entityCondition('entity_type', $bundle[0])
      ->entityCondition('bundle', $bundle[1]);

    if ($query_results = $query->execute()) {
      foreach ($query_results[$bundle[0]] as $entity_id => $item) {
        $results[$bundle[0]][$entity_id] = $query_results[$bundle[0]][$entity_id];
      }
    }
  }

  $options = array();
  foreach ($results as $entity_type => $items) {
    $entities = entity_load($entity_type, array_keys($items));
    foreach($entities as $entity_id => $entity) {
      $key = "$entity_type:$entity_id";
      $options[$key] = $entity->title . " ($key)";
    };
  }

  $form['title'] = array(
    '#markup' => sprintf("<h3>%s</h3>", $relation->label),
  );

  $form['target'] = array(
    '#type' => 'select',
    '#title' => t('Target'),
    '#options' => $options,
  );

  $relation_type = $relation->relation_type;
  $fields_info = field_info_instances('relation', $relation_type);
  $form['#parents'] = array(); // We just need this for field_default_form.
  foreach ($fields_info as $field_name => $field_info) {
    if ($field_name != 'endpoints') {
      $field = field_info_field($field_name);
      $field_instance = field_info_instance(
        'relation',
        $field_name,
        $relation_type
      );

      $form += field_default_form(
        'relation',
        NULL,
        $field,
        $field_instance,
        LANGUAGE_NONE,
        NULL,
        $form,
        $form_state
      );
    }
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Add Relation'),
    '#weight' => 100,
    //'#ajax' => TRUE, @todo: use ajax on these forms.
  );

  return $form;
}

/**
 * Form submit callback for the relation add form.
 */
function relation_creation_form_submit($form, &$form_state) {
  $relation_info = $form_state['build_info']['args'][0];
  $node = $form_state['build_info']['args'][1];

  $target = explode(":", $form_state['values']['target']);
  $endpoints = array(
    // @todo: abstract to any entity type
    array('entity_type' => 'node', 'entity_id' => $node->nid),
    array('entity_type' => $target[0], 'entity_id' => $target[1]),
  );
  $relation = relation_create($relation_info->relation_type, $endpoints);

  field_attach_submit('relation', $relation, $form, $form_state);

  try {
    field_attach_validate('relation', $relation);
  }
  catch (FieldValidationException $e) {
    foreach($e->errors as $field => $error) {
      foreach ($error[LANGUAGE_NONE] as $endpoint_error) {
        if (isset($endpoint_error[0]['error'])) {
          if ($endpoint_error[0]['error'] == 'relation_already_exists') {
            form_set_error($field, "You've already added this relationship.");
          }
          else {
            if (isset($endpoint_error[0]['message'])) {
              $error_message = $endpoint_error[0]['message'];
              form_set_error($field, $error_message);
              watchdog('relation_creation', $error_message);
            }
            else {
              form_set_error($field, 'Unknown error while creating entity');
              watchdog(
                'relation_creation',
                "Unknown Error: @e",
                array('@e' => $e));
            }
          }
        }
      }
    }
    return;
  }

  if (!relation_save($relation)) {
    form_set_error('Relation', "Unable to save sponsor.");
    watchdog('relation_creation', "Unable to save sponsor.");
  }
}

/**
 * Page access callback for the node relation tab.
 *
 * Check any permissions and validates that there are relation types where the
 * current node type can be its source.
 */
function relation_creation_access_callback($node, $permissions) {
  foreach ($permissions as $permission) {
    if (!user_access($permission)) {
      return FALSE;
    }
  }
  $available = relation_get_available_types('node', $node->type, 'source');
  return !empty($available);
}
