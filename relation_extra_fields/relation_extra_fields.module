<?php

/**
 * Implements hook_field_extra_fields().
 *
 * Provides extra fields on relation targetable entities to display relation
 * field values.
 */
function relation_extra_fields_field_extra_fields() {
  $bundles = field_info_bundles('relation');

  // Build out an array of our new extra fields.
  $extra_fields = array();
  foreach ($bundles as $bundle) {
    _relation_extra_fields_build_extra_fields($extra_fields, $bundle);
  }

  // Make sure that any new fields are hidden by default.
  $relation_types = array_keys($extra_fields['relation']);
  foreach ($relation_types as $relation_type) {
    $field_names = array_keys($extra_fields['relation'][$relation_type]['display']);
    foreach ($field_names as $field_name) {
      _relation_extra_fields_hide_new_field(
        'relation',
        $relation_type,
        $field_name
      );
    }
  }

  return $extra_fields;
}

/**
 * Implement hook_field_attach_view_alter().
 *
 * Attaches extra fields to relations.
 */
function relation_extra_fields_field_attach_view_alter(&$output, $context) {
  // Bail if this isn't a relation.
  if (!isset($context['entity_type']) || $context['entity_type'] != 'relation') {
    return;
  }

  $entity_type = $context['entity_type'];
  $entity = $context['entity'];
  $langcode = ($context['language']) ? $context['language'] : LANGUAGE_NONE;

  $extra_fields = field_info_extra_fields(
    $entity_type,
    $entity->relation_type,
    'display');

  $extra_fields = array_filter(
    $extra_fields,
    '_relation_extra_fields_filter_active_fields');

  foreach ($extra_fields as $field_name => $extra_field) {
    // Don't do anything if this isn't our extra field.
    if (!isset($extra_field['module']) || $extra_field['module'] != 'relation_extra_fields') {
      continue;
    }

    if (isset($extra_field['callback']) && function_exists($extra_field['callback'])) {
      $extra_field['callback']($output, $entity, $extra_field, $langcode);
    }
  }
}

function relation_extra_fields_extra_property_callback(&$output, $entity, $extra_field, $langcode) {
  $base_type = $extra_field['base instance']['entity_type'];
  $base_name = $extra_field['base instance']['field_name'];

  foreach ($entity->endpoints[$langcode] as $delta => $endpoint) {
    if ($endpoint['r_index'] != $extra_field['r_index']) {
      continue;
    }

    $entities = entity_load(
      $endpoint['entity_type'],
      array($endpoint['entity_id']));

    $loaded_entity = reset($entities);

    if (isset($loaded_entity->{$base_name})) {
      $field['#markup'] = $loaded_entity->{$base_name};
      $output[$extra_field['field_name']][$langcode][] = $field;
    }
  }
}

function relation_extra_fields_extra_field_callback(&$output, $entity, $extra_field, $langcode) {
  $base_type = $extra_field['base instance']['entity_type'];
  $base_name = $extra_field['base instance']['field_name'];

  foreach ($entity->endpoints[$langcode] as $delta => $endpoint) {
    if ($endpoint['r_index'] != $extra_field['r_index']) {
      continue;
    }

    $entities = entity_load(
      $endpoint['entity_type'],
      array($endpoint['entity_id']));

    $loaded_entity = reset($entities);

    if (isset($loaded_entity->{$base_name})) {
      $field = field_view_field(
        $base_type,
        $loaded_entity,
        $base_name,
        array('label' => 'hidden',),
        $langcode);

      $output[$extra_field['field_name']][$langcode][] = $field;
    }
  }
}

/**
 * Utility function that builds onto an extra_fields array by relation type.
 */
function _relation_extra_fields_build_extra_fields(&$extra_fields, $relation_type) {
  // This creates initiates a properly nested array and and sets a reference
  // var. This $fields var just exists to save on typing.
  $fields =& $extra_fields['relation'][$relation_type['relation_type']]['display'];

  // If the 'display' array hasn't already been instantiated, make it an array.
  if (!is_array($fields)) {
    $fields = array();
  }

  // This will add extra fields for each target bundle to the extra_fields
  // array.
  foreach ($relation_type['target_bundles'] as $target) {
    $fields += _relation_extra_fields_get_extra_fields($target, 1);
  }

  // This will add extra fields for each source bundle to the extra_fields
  // array.
  foreach ($relation_type['source_bundles'] as $target) {
    $fields += _relation_extra_fields_get_extra_fields($target, 0);
  }
}

/**
 * Utility function to build extra fields for each field of a relation
 * target/source.
 */
function _relation_extra_fields_get_extra_fields($target_type, $r_index) {
  // Get the entity_type and bundle from the string as stored by Relation.
  list($entity_type, $bundle) = explode(':', $target_type);

  // Get all instances from this bundle.
  $instances = field_info_instances($entity_type, $bundle);

  // Field label will be difference for target/source.
  $label_prefix = ($r_index) ? 'Relation Target:' : 'Relation Source:';

  // For each instance, build an extra field array.
  $fields = array();
  foreach ($instances as $field_name => $field_info) {
    // Field name will be difference for target/source.
    $extra_field_name = ($r_index) ? "relation_target_{$field_name}" : "relation_source_{$field_name}";

    // The actual extra field definition.
    $fields[$extra_field_name] = array(
      'label' => $label_prefix . " " . $field_info['label'] . " ({$target_type})",
      'description' => $field_info['description'],
      'weight' => 100,
      'field_name' => $extra_field_name,
      'module' => 'relation_extra_fields',
      'r_index' => $r_index,
      'base instance' => $field_info,
      'callback' => 'relation_extra_fields_extra_field_callback',
    );
  }

  $entity_info = entity_get_property_info($entity_type);

  if (isset($entity_info['properties']['title'])) {
    $field_name = 'title';
    $field_info = $entity_info['properties']['title'];

    $field_info['field_name'] = 'title';
    $field_info['entity_type'] = $entity_type;

    $extra_field_name = ($r_index) ? "relation_target_{$field_name}" : "relation_source_{$field_name}";

    if (isset($field_info['getter callback'])) {
      $callback = $field_info['getter callback'];
    }
    else {
      $callback = 'relation_extra_fields_extra_property_callback';
    }

    $fields[$extra_field_name] = array(
      'label' => $label_prefix . " " . $field_info['label'] . " ({$target_type})",
      'description' => $field_info['description'],
      'weight' => 100,
      'field_name' => $extra_field_name,
      'module' => 'relation_extra_fields',
      'r_index' => $r_index,
      'base instance' => $field_info,
      'callback' => $callback,
    );
  }

  return $fields;
}

/**
 * Utility function to hide any newly created extra_fields.
 */
function _relation_extra_fields_hide_new_field($entity_type, $bundle, $field_name) {
  $settings = field_bundle_settings($entity_type, $bundle);
  $settings_updated = FALSE;

  $entity_info = entity_get_info($entity_type, $bundle);
  $view_modes = array_keys($entity_info['view modes']);
  if (!in_array('default', $view_modes)) $view_modes[] = 'default';
  foreach ($view_modes as $view_mode) {
    if (!isset($settings['extra_fields']['display'][$field_name][$view_mode])) {
      $settings_updated = TRUE;
      $settings['extra_fields']['display'][$field_name][$view_mode] = array(
        'weight' => 100,
        'visible' => FALSE,
      );
    }
  }

  if ($settings_updated) {
    field_bundle_settings($entity_type, $bundle, $settings);
  }
}

/**
 * Callback function for array_filter in relation_extra_fields_entity_view().
 */
function _relation_extra_fields_filter_active_fields($item) {
  foreach ($item['display'] as $view_mode => $display_info) {
    if ($display_info['visible']) return TRUE;
  }

  return FALSE;
}
